/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit is the main interface of the application.
///
/// The UI accepts input from the user in the form of commands and forwards
/// them to the sequencer.
///
/// It displays relevant information in the main window.

#ifndef UI_H
#define UI_H

// Maximum length of a command input string.
#define CMD_LEN_MAX 256

#include <stdbool.h>
#include <stdint.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "config.h"

struct va_ui {
    struct va_evt *evt;
    struct va_seq *seq;
    struct va_config *config;

    SDL_Window *window;
    SDL_Renderer *renderer;

    TTF_Font *font;

    SDL_Color fg;
    SDL_Color bg;

    char cmd_buf[CMD_LEN_MAX];
    uint8_t cmd_buf_pos;

    /// UI requests to quit.
    bool quit;
};

/// Create a new UI.
struct va_ui * va_ui_create(struct va_seq *seq, struct va_config *config);

/// Free the UI.
void va_ui_free(struct va_ui *ui);

/// Process user input.
void va_ui_input(struct va_ui *ui);

/// Show the UI.
void va_ui_show(struct va_ui *ui);

#endif
