/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides instruments.

#ifndef INSTRUMENT_H
#define INSTRUMENT_H

#include <stdint.h>

#include "voice.h"

/// An instrument is a group of parameters for driving an oscillator.
struct instrument {
    /// Type of waveform.
    enum waveform waveform;
    /// Duty cycle of the waveform.
    double duty_cycle;
    /// Release part of ADSR envelope.
    uint8_t release;
};

#endif
