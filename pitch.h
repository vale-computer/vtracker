/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit converts named pitches to frequency values. 

#ifndef PITCH_H
#define PITCH_H

#include "error.h"

/// A pitch maps the name of a note (A4) to its frequency, in Hz (440.0).
struct pitch {
    /// Name, in name-octave notation.
    const char *pitch_name;
    /// Frequency, in Hz.
    double frequency;
};

/// Get a pitch structure for a named note.
result pitch_find(struct pitch *pitch, const char *pitch_name);

#endif
