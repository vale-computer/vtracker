# Welcome to vtracker

The VALE music tracker (vtracker) is a music sequencer for the VALE audio unit
(VAU). The VAU is the sound generation hardware for the forthcoming VALE8x64
computer.

# Quick start

To get started, run the vtracker program with one of the example programs as
follows:

./vtracker examples/<filename>.vap

The vtracker user interface appears. To start the sequencer, type :run and
press the Enter key. To stop it, type :stop. To quit vtracker, type :quit.

# Nomenclature

## Command

A command tells vtracker to do something. Commands are useful for automating
actions. For example, the :run command can be used in a VAP file to
automatically run the sequencer when the project is loaded.

## Instrument

An instrument determines the characteristics of a note's sound when it is
played. More precisely, an instrument is a group of parameters for driving a
VAU oscillator. These parameters are waveform, duty cycle, and
attack/sustain/decay/release (ADSR) envelope. NOTE: ADSR is not yet totally
supported in vtracker. There are up to 64 instruments available.

## Pattern

A pattern is a series of musical notes. Each note has a pitch, which
determines its frequency. Each note also has an instrument. A pattern can be
up to 64 notes long, and there are up to 64 patterns available.

## Project

A project (or tune, or song) is a piece of music. vtracker works with one
project at once. A project has a tempo (beats per minute), patterns,
instruments, tracks, and commands. 

## Sequencer

The sequencer steps through all the track, pattern, instrument and note data
from the beginning of the project to the end. At each step of each pattern, it
converts the data to signals for the VAU's oscillators and sends those signals
to the VAU emulator. vtracker delivers the mixed signals to the computer's
audio hardware, and the result is music your ears can enjoy.

## Track

A track represents a sequence of patterns that play over time. Each track can
play a different pattern. There are 4 tracks available in vtracker, one for
each VAU oscillator. This means 4 different patterns (as well as 4 different
notes) can play simultaneously at any given time. Each track can have a
sequence of up to 64 patterns from start to finish.

## VALE audio project (VAP) file

vtracker loads projects from/saves them to VALE audio project (VAP) files. VAP
files are text files, so take a look at the .vap files in the examples
directory.

# Workflow

These are the steps to create a project:

* Define instrument(s).
* Define patterns(s).
* Arrange pattern(s) into tracks.
* Enjoy the finished project.

In practice, you repeat these steps in an iterative process to get the right
result.

# User Interface

The user interface displays information about the current sequencer data. You
can enter commands at any time by typing the colon (:) character, followed by
the name of the command and its arguments, separated by spaces.

# Command reference

This is a list of commands available in vtracker. To save keystrokes, many
commands have a short version.

:load               Reload all data in the current project from its VAP file.
:l

:run                Run the sequencer.
:r

:stop               Stop the sequencer.
:s

:seek <integer>     Seek to a specific play position in the sequencer.

:l0 <integer>       Set the beginning loop marker.

:l1 <integer>       Set the ending loop marker.

:quit               Quit vtracker.
:q

