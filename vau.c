/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "vau.h"

#include <math.h>
#include <stdlib.h>

#include "log.h"
#include "voice.h"

struct vau * vau_create(uint32_t speed) {
    struct vau *vau = malloc(sizeof(struct vau));

    vau->voice_0 = va_voice_create(speed);
    vau->voice_1 = va_voice_create(speed);
    vau->voice_2 = va_voice_create(speed);
    vau->voice_3 = va_voice_create(speed);

    vau->speed = speed;

    log_info("Clock speed: %d Hz\n", vau->speed);

    va_voice_freq(vau->voice_0, 0.0);
    va_voice_freq(vau->voice_1, 0.0);
    va_voice_freq(vau->voice_2, 0.0);
    va_voice_freq(vau->voice_3, 0.0);

    return vau;
}

void vau_free(struct vau *vau) {
    va_voice_free(vau->voice_0);
    va_voice_free(vau->voice_1);
    va_voice_free(vau->voice_2);
    va_voice_free(vau->voice_3);

    free(vau);
    vau = NULL;
}

void vau_voice_init(struct vau *vau, uint8_t voice) {
    if (voice == 0) {
        va_voice_init(vau->voice_0);
    } else if (voice == 1) {
        va_voice_init(vau->voice_1);
    } else if (voice == 2) {
        va_voice_init(vau->voice_2);
    } else if (voice == 3) {
        va_voice_init(vau->voice_3);
    }
}

void vau_wave(struct vau *vau, uint8_t voice, enum waveform waveform,
    double duty_cycle) {

    if (voice == 0) {
        va_voice_wave(vau->voice_0, waveform, duty_cycle);
    } else if (voice == 1) {
        va_voice_wave(vau->voice_1, waveform, duty_cycle);
    } else if (voice == 2) {
        va_voice_wave(vau->voice_2, waveform, duty_cycle);
    } else if (voice == 3) {
        va_voice_wave(vau->voice_3, waveform, duty_cycle);
    }
}

double vau_freq_get(struct vau *vau, uint8_t voice) {
    if (voice == 0) {
        return vau->voice_0->frequency;
    } else if (voice == 1) {
        return vau->voice_1->frequency;
    } else if (voice == 2) {
        return vau->voice_2->frequency;
    } else if (voice == 3) {
        return vau->voice_3->frequency;
    } else {
        return 0.0;
    }
}

void vau_freq(struct vau *vau, uint8_t voice, double frequency) {
    log_debug("Voice %u set frequency %f\n", voice, frequency);

    if (voice == 0) {
        va_voice_freq(vau->voice_0, frequency);
    } else if (voice == 1) {
        va_voice_freq(vau->voice_1, frequency);
    } else if (voice == 2) {
        va_voice_freq(vau->voice_2, frequency);
    } else if (voice == 3) {
        va_voice_freq(vau->voice_3, frequency);
    }
}

uint8_t vau_ampl(struct vau *vau, uint8_t voice) {
    uint8_t res;

    if (voice == 0) {
        res = va_voice_ampl(vau->voice_0);
    } else if (voice == 1) {
        res = va_voice_ampl(vau->voice_1);
    } else if (voice == 2) {
        res = va_voice_ampl(vau->voice_2);
    } else if (voice == 3) {
        res = va_voice_ampl(vau->voice_3);
    }

    return res;
}

void vau_step(struct vau *vau) {
    va_voice_step(vau->voice_0);
    va_voice_step(vau->voice_1);
    va_voice_step(vau->voice_2);
    va_voice_step(vau->voice_3);
}

void vau_adsr(struct vau *vau, uint8_t voice, uint8_t attack, uint8_t decay,
    uint8_t sustain, uint8_t release) {

    log_info("Voice %d set adsr %u %u %u %u.\n", voice, attack, decay, sustain,
        release);

    if (voice == 0) {
        va_voice_adsr(vau->voice_0, attack, decay, sustain, release);
    } else if (voice == 1) {
        va_voice_adsr(vau->voice_1, attack, decay, sustain, release);
    } else if (voice == 2) {
        va_voice_adsr(vau->voice_2, attack, decay, sustain, release);
    } else if (voice == 3) {
        va_voice_adsr(vau->voice_3, attack, decay, sustain, release);
    }
}
