/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides musical notes and effects.

#ifndef NOTE_H
#define NOTE_H

#include <stdint.h>

#include "pitch.h"

/// An effect is a procedure applied to a track to change its sound.
enum effect {
    NONE,
    PITCH_SLIDE,
};

/// A musical note.
struct note {
    /// Index of the instrument.
    uint8_t instrument_id;
    /// Pitch.
    struct pitch pitch;
    /// Effect.
    enum effect effect;
    /// Value of the effect to apply.
    uint8_t effect_val;
};

#endif
