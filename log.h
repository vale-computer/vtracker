/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides logging facilities.

#ifndef LOG_H
#define LOG_H

#include <stdint.h>
#include <stdio.h>

// Logging levels.
enum log_levels {ERR, INFO, DEBUG, TRACE};

// Logging macros.
#define log_stdout(...) printf(__VA_ARGS__)
#define log_stderr(...) fprintf(stderr, __VA_ARGS__)

#define log_error(...) \
    if (log_level >= ERR) { \
        log_stderr("ERROR: "); \
        log_stderr(__VA_ARGS__); \
    }
#define log_info(...) \
    if (log_level >= INFO) { \
        log_stderr("INFO: "); \
        log_stderr(__VA_ARGS__); \
    }
#define log_debug(...) \
    if (log_level >= DEBUG) { \
        log_stderr("DEBUG: "); \
        log_stderr(__VA_ARGS__); \
    }
#define log_trace(...) \
    if (log_level >= TRACE) { \
        log_stderr("TRACE: "); \
        log_stderr(__VA_ARGS__); \
    }

/// Global logging level. It is required to be assigned elsewhere.
extern uint8_t log_level;

#endif
