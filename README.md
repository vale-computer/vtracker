# About

The VALE music tracker (vtracker) is a music sequencer for the VALE audio unit
(VAU). The VAU is the sound generation module for the forthcoming VALE8x64
computer.

# Project status

Very early. Project (VAP) files can be handwritten and played back with the
vtracker program. Instrument capabilities are quite limited since the VAU
itself is early in development.

# Dependencies

* SDL2

# Build and usage

```
make
./vtracker example/<filename>.vap
```

See manual.txt for more information on usage.

# VAU project status

Also very early. vtracker is part of a top-down approach to developing VAU.
The idea is to arrive at a desired feature set and behavioral model of the
hardware through use of the emulator in vtracker, then move to describing it in
HDL.
