/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This units supports configuration for the application.

#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "error.h"

struct va_config {
    ///  This is the number of steps to execute for each run of the audio
    ///  system.
    uint32_t sample_ahead;
    /// This is the calendar duration of audio time left in the output buffer at
    /// which the application triggers another run of the audio system.
    long int sample_at;
    /// Are application windows resizable?
    bool window_resizable;
};

/// Load configuration from a file.
result config_load(struct va_config *config, const char *filename);

/// Write default configuration to a file.
result config_default(const char *filename);

#endif
