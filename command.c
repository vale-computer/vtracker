/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "command.h"

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "error.h"

result va_cmd_from_str(char *cmd_str, struct command *command) {
    result res = OK;

    char *tok = strtok(cmd_str, " ");

    if (tok != NULL) {
        if (strcasecmp(cmd_str, ":load") == 0 ||
            strcasecmp(cmd_str, ":l") == 0) {

            command->type = CMD_LOAD;
        } else if (strcasecmp(cmd_str, ":quit") == 0 ||
            strcasecmp(cmd_str, ":q") == 0) {

            command->type = CMD_QUIT;
        } else if (strcasecmp(cmd_str, ":run") == 0 ||
            strcasecmp(cmd_str, ":r") == 0) {

            command->type = CMD_RUN;
        } else if (strcasecmp(cmd_str, ":stop") == 0 ||
            strcasecmp(cmd_str, ":s") == 0) {

            command->type = CMD_STOP;
        } else if (strcasecmp(cmd_str, ":l0") == 0) {
            command->type = CMD_LOOP0;
        } else if (strcasecmp(cmd_str, ":l1") == 0) {
            command->type = CMD_LOOP1;
        } else if (strcasecmp(cmd_str, ":seek") == 0) {
            command->type = CMD_SEEK;
        } else {
            res = ERROR_BOUNDS;
        }

        for (int i = 0; i < NUM_CMD_ARGS; ++i) {
            if ((tok = strtok(NULL, " ")) == NULL) {
                break;
            }

            uint32_t arg = strtol(tok, NULL, 0);

            command->arguments[i] = arg;
            command->args_len += 1;
        }
    } else {
        res = ERROR_BOUNDS;
    }

    return res;
}
