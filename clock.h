/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit emulates an oscillator used as a hardware clock. The clock
/// supports converting the emulated time to calendar time.
///
/// This implementation is currently tested only for 1 MHz frequency. Elapsed
/// time is accurate only when the result of us_per_clock is an integer.

#ifndef CLOCK_H
#define CLOCK_H

#include <stdbool.h>
#include <stdint.h>
#include <sys/time.h>

struct va_clock {
    /// Frequency, in Hz.
    uint32_t frequency;
    /// Elapsed calendar time since started.
    struct timeval elapsed_time;

    /// Number of microseconds per each clock cycle.
    uint32_t us_per_clock;

    uint32_t step;
};

/// Create a new clock.
struct va_clock * va_clock_create(uint32_t frequency);

/// Free the clock.
void va_clock_free(struct va_clock *clock);

/// Step the clock for one cycle.
uint32_t va_clock_step(struct va_clock *clock);

#endif
