/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "clock.h"

#include <stdlib.h>

#include "log.h"

struct va_clock * va_clock_create(uint32_t frequency) {
    struct va_clock *clock = malloc(sizeof(struct va_clock));

    clock->frequency = frequency;

    clock->elapsed_time.tv_sec = 0;
    clock->elapsed_time.tv_usec = 0;

    clock->us_per_clock = 1000000 / clock->frequency;

    clock->step = 0;

    return clock;
}

void va_clock_free(struct va_clock *clock) {
    free(clock);
    clock = NULL;
}

uint32_t va_clock_step(struct va_clock *clock) {
    uint32_t res = clock->step;

    clock->step += 1;

    clock->elapsed_time.tv_usec += clock->us_per_clock;

    if (clock->step == clock->frequency) {
        clock->step = 0;

        clock->elapsed_time.tv_sec += 1;
        clock->elapsed_time.tv_usec = 0;
    }

    log_trace("Clock step %u elapsed time s %li us %li.\n", clock->step,
        clock->elapsed_time.tv_sec, clock->elapsed_time.tv_usec);

    return res;
}
