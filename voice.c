/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdlib.h>

#include "log.h"
#include "voice.h"

// Table of values representing one period of a triangle wave.
uint8_t triangle_wave_table[] = {
    128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
    144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
    160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
    176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
    192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
    208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
    224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
    254, 253, 252, 251, 250, 249, 248, 247, 246, 245, 244, 243, 242, 241, 240, 239,
    238, 237, 236, 235, 234, 233, 232, 231, 230, 229, 228, 227, 226, 225, 224, 223,
    222, 221, 220, 219, 218, 217, 216, 215, 214, 213, 212, 211, 210, 209, 208, 207,
    206, 205, 204, 203, 202, 201, 200, 199, 198, 197, 196, 195, 194, 193, 192, 191,
    190, 189, 188, 187, 186, 185, 184, 183, 182, 181, 180, 179, 178, 177, 176, 175,
    174, 173, 172, 171, 170, 169, 168, 167, 166, 165, 164, 163, 162, 161, 160, 159,
    158, 157, 156, 155, 154, 153, 152, 151, 150, 149, 148, 147, 146, 145, 144, 143,
    142, 141, 140, 139, 138, 137, 136, 135, 134, 133, 132, 131, 130, 129, 128, 127,
    126, 125, 124, 123, 122, 121, 120, 119, 118, 117, 116, 115, 114, 113, 112, 111,
    110, 109, 108, 107, 106, 105, 104, 103, 102, 101, 100,  99,  98,  97,  96,  95,
     94,  93,  92,  91,  90,  89,  88,  87,  86,  85,  84,  83,  82,  81,  80,  79,
     78,  77,  76,  75,  74,  73,  72,  71,  70,  69,  68,  67,  66,  65,  64,  63,
     62,  61,  60,  59,  58,  57,  56,  55,  54,  53,  52,  51,  50,  49,  48,  47,
     46,  45,  44,  43,  42,  41,  40,  39,  38,  37,  36,  35,  34,  33,  32,  31,
     30,  29,  28,  27,  26,  25,  24,  23,  22,  21,  20,  19,  18,  17,  16,  15,
     14,  13,  12,  11,  10,   9,   8,   7,   6,   5,   4,   3,   2,   1,   0,   1,
      2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,  16,  17,
     18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  32,  33,
     34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
     50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,  64,  65,
     66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,
     82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,  96,  97,
     98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113,
    114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
};

#define triangle_wave_table_len() sizeof(triangle_wave_table)

uint8_t sawtooth_wave_table[] = {
    128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
    144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
    160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
    176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191,
    192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207,
    208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
    224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,  15,
     16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,
     32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
     48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
     64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
     80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  91,  92,  93,  94,  95,
     96,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
    112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
};

#define sawtooth_wave_table_len() sizeof(sawtooth_wave_table)

struct va_voice * va_voice_create(uint32_t clock_freq) {
    struct va_voice *voice = malloc(sizeof(struct va_voice));

    voice->wave = PULSE;
    voice->amplitude = 0;
    voice->frequency = 0;

    voice->duty_cycle = 0.5;

    voice->level = 1.0;

    voice->release = 0xf;
    voice->release_step = 0;

    voice->step = 0;

    voice->clock_freq = clock_freq;

    return voice;
}

void va_voice_init(struct va_voice *voice) {
    voice->wave = PULSE;
    voice->amplitude = 0;
    voice->frequency = 0;

    voice->level = 1.0;

    voice->release = 0xf;
    voice->release_step = 0;

    voice->step = 0;
}

void va_voice_free(struct va_voice *voice) {
    free(voice);
    voice = NULL;
}

void va_voice_wave(struct va_voice *voice, enum waveform waveform,
    double duty_cycle) {

    voice->wave = waveform;
    voice->duty_cycle = duty_cycle;
}

void va_voice_freq(struct va_voice *voice, double frequency) {
    voice->frequency = frequency;
}


uint8_t va_voice_ampl(struct va_voice *voice) {
    if (voice->level < 0) {
        log_error("ERROR VOICE\n");
    }
    return voice->amplitude * voice->level;
}

void va_voice_step(struct va_voice *voice) {
    // Samples per clock cycle depends on the type of waveform, its frequency,
    // and its resolution (smoothness). Examples assuming 1 MHz clock.
    //
    // A pulse wave has 1 MHz / 440 Hz == 2272.7273 samples per clock cycle.

    //
    double samples_per_clock = voice->clock_freq / voice->frequency;

    if (voice->wave == TRIANGLE) {
        if (voice->step >= samples_per_clock - 1) {
            voice->step = 0;
        }

        int wavetable_pos = voice->step / samples_per_clock *
            triangle_wave_table_len();

        voice->amplitude = triangle_wave_table[wavetable_pos];

        log_trace("Triangle res %lu step %lu spc %f pos %d ampl %u.\n",
            triangle_wave_table_len(), voice->step, samples_per_clock,
            wavetable_pos, triangle_wave_table[wavetable_pos]);
    } else if (voice->wave == SAWTOOTH) {
        if (voice->step >= samples_per_clock - 1) {
            voice->step = 0;
        }

        int wavetable_pos = voice->step / samples_per_clock *
            sawtooth_wave_table_len();

        voice->amplitude = sawtooth_wave_table[wavetable_pos];

        log_trace("Sawtooth res %lu step %lu spc %f pos %d ampl %u.\n",
            triangle_wave_table_len(), voice->step, samples_per_clock,
            wavetable_pos, sawtooth_wave_table[wavetable_pos]);
    } else if (voice->wave == PULSE) {
        if (voice->step >= samples_per_clock - 1) {
            voice->step = 0;
        }

        if (voice->step <= (voice->duty_cycle * samples_per_clock)) {
            voice->amplitude = 0xff;
        } else {
            voice->amplitude = 0;
        }
    } else if (voice->wave == NOISE) {
        if (voice->step >= samples_per_clock - 1) {
            voice->amplitude = rand() % 256;
            voice->step = 0;
        }
    }

    voice->step += 1;

    if (voice->release != 0) {
        voice->release_step += 1;

        if ((voice->release_step >=
            (voice->clock_freq / 512) * voice->release) &&
            voice->level > 0.0) {

            voice->release_step = 0;

            voice->level -= 0.1;
            if (voice->level <= 0.0) {
                log_trace("note off level %f\n", voice->level);
                voice->level = 0.0;
            }
        }
    }
}

void va_voice_adsr(struct va_voice *voice, uint8_t attack, uint8_t decay,
    uint8_t sustain, uint8_t release) {

    voice->level = 1.0;

    voice->release = release;
    voice->release_step = 0;
}
