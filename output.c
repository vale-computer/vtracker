/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "output.h"

#include <SDL2/SDL.h>

#include "error.h"
#include "log.h"

struct va_output * va_output_create(uint32_t clock_freq) {
    result res = OK;

    struct va_output * output = malloc(sizeof(struct va_output));

    output->clock_freq = clock_freq;
    output->clock = 0;
    output->bufpos = 0;

    SDL_memset(&output->want, 0, sizeof(output->want));
    output->want.freq = SAMPLES_PER_SEC;
    output->want.format = AUDIO_U8;
    output->want.channels = 1;
    output->want.callback = NULL;

    output->dev = SDL_OpenAudioDevice(NULL, 0, &output->want,
        &output->have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);

    if (output->dev == 0) {
        log_error("Failed to open audio: %s", SDL_GetError());
        res = ERROR_OTHER;
    }

    if (output->have.format != output->want.format) {
        log_error("Didn't get U8 audio format.");
        res = ERROR_OTHER;
    }

    if (res != OK) {
        va_output_free(output);
    }

    return output;
}

void va_output_free(struct va_output *output) {
    SDL_CloseAudioDevice(output->dev);

    free(output);
    output = NULL;
}

void va_output_play(struct va_output *output) {
    SDL_PauseAudioDevice(output->dev, 0);
}

void va_output_sample(struct va_output *output, uint8_t sample) {

    // An input sample comes at every clock pulse; at a rate of CLOCK_HZ.
    // Downsample to a rate of SAMPLES_PER_SEC for output.

    if (output->clock % (output->clock_freq / SAMPLES_PER_SEC) == 0) {
        output->sdl_buffer[output->bufpos] = sample;
        output->bufpos += 1;
        output->clock = 0;
    }

    output->clock += 1;

    if (output->bufpos >= SDL_BUFFER_LEN) {
        log_debug("Queue audio at bufpos %d.\n", output->bufpos);

        int res = SDL_QueueAudio(output->dev, (void *) &output->sdl_buffer,
            output->bufpos * sizeof(uint8_t));

        if (res != 0) {
            log_error("Failed to queue audio to device.\n");
        }

        output->bufpos = 0;
    }
}
