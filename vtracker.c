/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "vau.h"

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include "clock.h"
#include "config.h"
#include "error.h"
#include "log.h"
#include "output.h"
#include "sdl.h"
#include "sequencer.h"
#include "time.h"
#include "ui.h"
#include "vau.h"

#define CLOCK_HZ 1000000

const char *config_filename = "vtracker.cfg";

/*
Global state.
*/

uint8_t log_level = ERR;

void print_usage() {
    printf("Usage: vtracker [options] file\n");
    printf("Options:\n");
    printf("  -v[vv]    Raise logging verbosity.\n");
}

static result config_get(struct va_config *config) {
    result res = OK;

    access(config_filename, F_OK);

    if (errno == ENOENT) {
        res = config_default(config_filename);
    }

    if (res == OK) {
        res = config_load(config, config_filename);
    }

    return res;
}

int main(int argc, char *argv[]) {
    result res = OK;
    int exit_code = 0;

    // Process program arguments.

    const char *filename = NULL;

    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0) {
            print_usage();
            exit(0);
        } else if (strcmp(argv[i], "-v") == 0) {
            log_level = INFO;
            log_info("Log level: INFO\n");
        } else if (strcmp(argv[i], "-vv") == 0) {
            log_level = DEBUG;
            log_info("Log level: DEBUG\n");
        } else if (strcmp(argv[i], "-vvv") == 0) {
            log_level = TRACE;
            log_info("Log level: TRACE\n");
        } else {
            filename = argv[i];
        }
    }

    struct va_config config;

    if ((res = config_get(&config)) != OK) {
        exit_code = res;
        goto exit;
    }

    if (filename == NULL) {
        log_error("Filename is required.\n");
        exit_code = ERROR_IO;
        goto exit;
    }

    printf("Welcome to vtracker.\n");

    struct va_clock *clock = NULL;
    struct vau *vau = NULL;
    struct va_output *output = NULL;
    struct va_seq *seq = NULL;
    struct va_ui *ui = NULL;

    clock = va_clock_create(CLOCK_HZ);
    vau = vau_create(clock->frequency);

    res = sdl_init();

    if (res != OK) {
        exit_code = ERROR_IO;
        goto exit;
    }

    output = va_output_create(clock->frequency);

    if (output == NULL) {
        res = ERROR_OTHER;
        exit_code = res;
        goto exit;
    }

    va_output_play(output);

    seq = va_seq_create(vau, clock->frequency);
    ui = va_ui_create(seq, &config);

    res = va_seq_load(seq, filename);

    if (res != OK) {
        exit_code = ERROR_IO;
        goto exit;
    }

    struct timeval diff;
    diff.tv_sec = 0;
    diff.tv_usec = 0;

    struct timeval start_time;
    start_time.tv_sec = 0;
    start_time.tv_usec = 0;

    gettimeofday(&start_time, NULL);

    while (!ui->quit) {
        struct timeval now;
        gettimeofday(&now, NULL);

        struct timeval elapsed;
        timeval_subtract(&elapsed, &now, &start_time);

        timeval_subtract(&diff, &clock->elapsed_time, &elapsed);
        log_debug("Diff s %li us %li.\n", diff.tv_sec, diff.tv_usec);

        if (diff.tv_sec < 0 || (diff.tv_sec < 1 && diff.tv_usec <= config.sample_at)) {
            log_info("Buffer diff s %li us %li.\n", diff.tv_sec,
                diff.tv_usec);

            for (int i = 0; i < config.sample_ahead; ++i) {
                va_clock_step(clock);

                va_seq_step(seq);

                uint8_t mixed = va_seq_sample(seq);

                va_output_sample(output, mixed);
            }
        }

        va_ui_input(ui);
        va_ui_show(ui);
    }

exit:
    log_info("Release resources.\n");

    vau_free(vau);
    va_ui_free(ui);
    va_output_free(output);
    va_seq_free(seq);
    va_clock_free(clock);

    sdl_free();

    printf("Bye. :)\n");

    return exit_code;
}
