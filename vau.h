/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit emulates the VALE Audio Unit (VAU). The VAU is the
/// sound generation hardware for the forthcoming VALE8x64 computer.

#ifndef VAU_H
#define VAU_H

#include <stdint.h>

#include "voice.h"

/*
Divides clock to get the desired frequency. If clock signal is 1000 Hz and
desired frequency is 440 Hz, 1000000/440 = 2272.7272
*/

struct vau {
    struct va_voice *voice_0;
    struct va_voice *voice_1;
    struct va_voice *voice_2;
    struct va_voice *voice_3;

    // Clock speed, in Hz.
    uint32_t speed;
};

/// Create a new VAU.
struct vau * vau_create(uint32_t speed);

/// Free the VAU.
void vau_free(struct vau *vau);

/// Update the state of the VAU.
void vau_step(struct vau *vau);

/// Set the waveform of a voice.
void vau_wave(struct vau *vau, uint8_t voice, enum waveform waveform,
    double duty_cycle);

/// Get the frequency of a voice.
double vau_freq_get(struct vau *vau, uint8_t voice);

/// Initialize a voice.
void vau_voice_init(struct vau *vau, uint8_t voice);

/// Set the frequency of a voice.
void vau_freq(struct vau *vau, uint8_t voice, double frequency);

/// Get a sample of audio from a voice.
uint8_t vau_ampl(struct vau *vau, uint8_t voice);

/// Set the attack/decay/sustain/release (ADSR) envelope of a voice.
void vau_adsr(struct vau *vau, uint8_t voice, uint8_t attack, uint8_t decay,
    uint8_t sustain, uint8_t release);

#endif
