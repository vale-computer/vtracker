/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "vap_parser.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "command.h"
#include "error.h"
#include "log.h"
#include "sequencer.h"
#include "vap.h"

#define VAP_LINE_LEN_MAX 256

// Sections of the .vap file format.

enum file_structure {
    FS_NONE,
    PATTERN,
    INSTRUMENT,
    COMMAND_LIST,
};

result va_vap_seq_load(struct va_seq *seq, const char *filename) {
    log_info("Loading sequencer from %s.\n", filename);

    result res = OK;

    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        log_error("Failed to open %s.\n", filename);
        res = ERROR_IO;
    }

    enum file_structure file_structure = FS_NONE;

    char line[VAP_LINE_LEN_MAX];

    int line_num = 0;

    long int instrument_id;
    enum waveform waveform;
    int release;
    double pulse_duty_cycle = 0.5;

    if (res == OK) {
        while (fgets(line, VAP_LINE_LEN_MAX, file)) {
            int len = strlen(line);
            line[len - 1] = 0;

            char *tail;

            log_debug("Parse line %d %s\n", line_num, line);

            line_num += 1;

            if (strlen(line) == 0 || line[0] == ';') {
                log_debug("Skip empty or comment line.\n");
                continue;
            }

            char *tok = strtok(line, " ");

            if (strcasecmp(tok, "inst") == 0) {
                char *in_id = strtok(NULL, " ");

                instrument_id = strtol(in_id, &tail, 0);

                if (tail[0] == 0) {
                    file_structure = INSTRUMENT;

                    log_debug("Instrument start %li.\n", instrument_id);
                } else {
                    log_error("Invalid instrument idx %s.\n", in_id);

                    res = ERROR_BOUNDS;
                    break;
                }
            } else if (file_structure == INSTRUMENT) {
                if (strcasecmp(tok, "end") == 0) {
                    log_debug("Waveform %d duty cycle %f release %d.\n",
                        waveform, pulse_duty_cycle, release);

                    res = va_vap_inst_create(&seq->vap, instrument_id, waveform,
                        pulse_duty_cycle, release);

                    if (res != OK) {
                        log_error("Failed to create instrument idx %li.\n", instrument_id);
                        break;
                    }

                    log_debug("Instrument end %li.\n", instrument_id);
                } else if (strcasecmp(tok, "wave") == 0) {
                    char *in_wave = strtok(NULL, " ");

                    if (strcasecmp(in_wave, "pulse") == 0) {
                        waveform = PULSE;

                        char *in_duty_cycle = strtok(NULL, " ");

                        if (in_duty_cycle != NULL) {
                            pulse_duty_cycle = strtod(in_duty_cycle, &tail);

                            if (tail[0] != 0) {
                                pulse_duty_cycle = 0.5;
                            }
                        }
                    } else if (strcasecmp(in_wave, "sawtooth") == 0) {
                        waveform = SAWTOOTH;
                    } else if (strcasecmp(in_wave, "triangle") == 0) {
                        waveform = TRIANGLE;
                    } else if (strcasecmp(in_wave, "noise") == 0) {
                        waveform = NOISE;
                    }
                } else if (strcasecmp(tok, "sr") == 0) {
                    char *in_sr = strtok(NULL, " ");

                    long int sr = strtol(in_sr, NULL, 16);

                    release = sr & 0xf;
                }
            }
        }
    }

    if (file != NULL) {
        if (fclose(file) != 0) {
            log_error("Failed to close %s.\n", filename);
            res = ERROR_IO;
        }
    }

    return res;
}

result va_vap_track_load(struct va_seq *seq, const char *filename) {
    log_info("Loading track from %s.\n", filename);

    result res = OK;

    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        log_error("Failed to open %s.\n", filename);
        res = ERROR_IO;
    }

    enum file_structure file_structure = FS_NONE;

    char line[VAP_LINE_LEN_MAX];
    char cmd_str[VAP_LINE_LEN_MAX];

    int line_num = 0;

    uint8_t pattern_id;

    struct command commands[NUM_COMMANDS];
    int start_commands_len = 0;

    if (res == OK) {
        while (fgets(line, VAP_LINE_LEN_MAX, file)) {
            int len = strlen(line);
            line[len - 1] = 0;

            char *tail;

            log_debug("Parse line %d %s\n", line_num, line);

            line_num += 1;

            if (strlen(line) == 0 || line[0] == ';') {
                log_debug("Skip empty or comment line.\n");
                continue;
            }

            strcpy(cmd_str, line);

            char *tok = strtok(line, " ");

            if (strcasecmp(tok, "bpm") == 0) {
                char *in_bpm = strtok(NULL, " ");
                long int bpm = strtol(in_bpm, &tail, 0);

                if (tail[0] == 0) {
                    log_info("Beats per minute %ld.\n", bpm);

                    seq->vap.beats_per_min = bpm;
                } else {
                    log_error("Invalid bpm %s.\n", in_bpm);
                }
            } else if (strcasecmp(tok, "pat") == 0) {
                char *pattern_name = strtok(NULL, " ");

                res = va_vap_pattern_get(&seq->vap, pattern_name, NULL);

                if (res == OK) {
                    log_error("Pattern %s already defined.\n", pattern_name);
                    res = ERROR_BOUNDS;
                    break;
                }

                res = va_vap_pattern_add(&seq->vap, pattern_name, &pattern_id);

                if (res != OK) {
                    log_error("Failed to add pattern %s.\n", pattern_name);
                    break;
                }

                file_structure = PATTERN;

                log_debug("Pattern %s start.\n", pattern_name);
            } else if (strcasecmp(tok, "track") == 0) {
                char *in_track_id = strtok(NULL, " ");
                long int track_id = strtol(in_track_id, &tail, 0);

                if (tail[0] == 0) {
                    struct va_track *track = &seq->tracks[track_id];
                    struct pattern_seq *pattern_seq = &seq->vap.pattern_seq[track_id];

                    track->enabled = true;

                    while ((tok = strtok(NULL, " ")) != NULL) {
                        const char *pattern_name = tok;

                        log_info("Track %ld seq %d pattern %s.\n",
                            track_id, pattern_seq->len, pattern_name);

                        res = va_vap_pattern_get(&seq->vap, pattern_name,
                            &pattern_id);

                        if (res != OK) {
                            log_error("Pattern %s does not exist.\n", pattern_name);
                            break;
                        }

                        res = va_vap_pattern_seq_add(pattern_seq, pattern_id);

                        if (res != OK) {
                            log_error("Failed to add pattern %d to track %li.\n",
                                pattern_id, track_id);
                            break;
                        }
                    }

                    if (res != OK) {
                        break;
                    }

                    log_info("Len %d.\n", len);
                } else {
                    log_error("Invalid track idx %s.\n", in_track_id);

                    res = ERROR_BOUNDS;
                    break;
                }
            } else if (strcasecmp(tok, "commands") == 0) {
                file_structure = COMMAND_LIST;

                log_debug("Command list start.\n");
            } else if (file_structure == PATTERN) {
                if (strcasecmp(tok, "end") == 0) {
                    log_debug("Pattern end %d.\n", pattern_id);

                    file_structure = FS_NONE;
                } else if (strcmp(tok, "..") == 0) {
                    log_debug("Rest.\n");
                    res = va_vap_pattern_rest_add(&seq->vap, pattern_id);

                    if (res != OK) {
                        log_error("Failed to add rest, pattern %d\n",
                            pattern_id);
                        break;
                    }
                } else {
                    const char *pitch_name = tok;

                    tok = strtok(NULL, " ");

                    if (tok == NULL) {
                        log_error("Instrument idx missing.\n");
                        res = ERROR_BOUNDS;
                        break;
                    }
                    long int instrument_id = strtol(tok, NULL, 0);
                    tok = strtok(NULL, " ");
                    long int effect_val = 0;

                    enum effect effect;

                    if (tok != NULL) {
                        effect_val = strtol(tok, NULL, 0);
                        effect = PITCH_SLIDE;
                    } else {
                        effect = NONE;
                    }

                    log_debug("Pitch %s instrument_id %ld.\n", pitch_name,
                        instrument_id);

                    res = va_vap_pattern_note_add(&seq->vap, pattern_id,
                        instrument_id, pitch_name, effect, effect_val);

                    if (res != OK) {
                        log_error("Failed to add note, pattern %d instrument idx %li pitch %s effect %d val %li.\n",
                            pattern_id, instrument_id, pitch_name,
                            effect, effect_val);
                        break;
                    }
                }
            } else if (file_structure == COMMAND_LIST) {
                if (strcasecmp(tok, "end") == 0) {
                    log_debug("Command list end.\n");
                } else {
                    log_info("line %s\n", cmd_str);
                    res = va_cmd_from_str(cmd_str, &commands[start_commands_len]);

                    if (res != OK) {
                        log_error("Failed to process start command %s.\n", tok);
                    } else {
                        start_commands_len += 1;
                    }
                }

            }
        }
    }

    if (res == OK) {
        // Pattern sequences changed; seek to beginning of track.
        va_seq_seek(seq, 0);

        log_info("Execute start commands.\n");

        for (int cmd_idx = 0; cmd_idx < start_commands_len; ++cmd_idx) {
            va_seq_exec(seq, commands[cmd_idx]);
        }
    }

    if (file != NULL) {
        if (fclose(file) != 0) {
            log_error("Failed to close %s.\n", filename);
            res = ERROR_IO;
        }
    }

    return res;
}

