/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides the VALE audio project (VAP). A project is the
/// context for the vtracker application and corresponds to a "song"
/// or "tune".

#ifndef VAP_H
#define VAP_H

#include <stdint.h>

#include "error.h"
#include "instrument.h"
#include "note.h"
#include "pattern.h"

// Maximum length of a pattern sequence.
#define PATTERN_SEQ_LEN 64
// Total number of available instruments.
#define NUM_INSTRUMENTS 64
// Total number of available patterns.
#define NUM_PATTERNS 64
// Total number of tracks.
#define NUM_TRACKS 4

/// A pattern sequence is an ordered sequence of pattern indices which can be
/// up to PATTERN_SEQ_LEN in length.
struct pattern_seq {
    // Sequence of pattern indices.
    uint8_t pattern_ids[PATTERN_SEQ_LEN];
    // Number of patterns (length of sequence).
    uint8_t len;
};

struct va_vap {
    /// Instruments.
    struct instrument instruments[NUM_INSTRUMENTS];

    /// Patterns.
    struct pattern patterns[NUM_PATTERNS];

    /// Pattern sequence for each track.
    struct pattern_seq pattern_seq[NUM_TRACKS];

    uint16_t beats_per_min;
};

/// Initialize it. Call this before doing anything else.
void va_vap_init(struct va_vap *vap);

/// Reset the instruments to default state;
void va_vap_inst_reset(struct va_vap *vap);

/// Reset the patterns to default state;
void va_vap_patterns_reset(struct va_vap *vap);

/// Reset the pattern sequences to default state;
void va_vap_pattern_seq_reset(struct va_vap *vap);

/// Create a new instrument and add it to the VAP project.
result va_vap_inst_create(struct va_vap *vap, uint8_t id,
    enum waveform waveform, double duty_cycle, uint8_t release);

/// Get the id of the pattern with the specified name. Return it in pattern_id.
result va_vap_pattern_get(struct va_vap *vap, const char *name,
    uint8_t *pattern_id);

/// Add a pattern with the specified name. Return the new id in pattern_id.
result va_vap_pattern_add(struct va_vap *vap, const char *name,
    uint8_t *pattern_id);

/// Add a note to a pattern in the VAP project.
result va_vap_pattern_note_add(struct va_vap *vap, uint8_t pattern_id,
    uint8_t instrument_id, const char *pitch_name, enum effect effect,
    uint8_t effect_val);

/// Add a rest to a pattern in the VAP project.
result va_vap_pattern_rest_add(struct va_vap *vap, uint8_t pattern_idx);

/// Add a pattern to a pattern sequence in the VAP project.
result va_vap_pattern_seq_add(struct pattern_seq *pattern_seq,
    uint8_t pattern_idx);

#endif
