/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ui.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "command.h"
#include "error.h"
#include "log.h"
#include "sequencer.h"

#define OUTPUT_BUF_LEN 256

// TODO: These would change when the font or font size changes, so we need to
// calculate them by measuring a character of the font.
#define CHAR_WIDTH_PX 8
#define CHAR_HEIGHT_PX 18

static const int font_size = 16;
static const int win_width = 1024;
static const int win_height = 768;

static void x_y_from_col_row(uint16_t col, uint16_t row, int *x, int *y) {
    *x = col * CHAR_WIDTH_PX;
    *y = row * CHAR_HEIGHT_PX;
}

static void draw_text(struct va_ui *ui, int x, int y, const char *text) {
    SDL_Surface *surface = TTF_RenderText_Shaded(ui->font, text, ui->fg,
        ui->bg);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(ui->renderer, surface);

    SDL_Rect text_rect;
    text_rect.x = x;
    text_rect.y = y;
    text_rect.w = surface->w;
    text_rect.h = surface->h;

    SDL_FreeSurface(surface);
    SDL_RenderCopy(ui->renderer, texture, NULL, &text_rect);
    SDL_DestroyTexture(texture);
}

static void cmd_buf_add(struct va_ui *ui, char c) {
    if (ui->cmd_buf_pos < CMD_LEN_MAX) {
        ui->cmd_buf[ui->cmd_buf_pos] = c;
        ui->cmd_buf_pos += 1;
    }
}

static void cmd_buf_remove(struct va_ui *ui) {
    if (ui->cmd_buf_pos > 0) {
        ui->cmd_buf_pos -= 1;
        ui->cmd_buf[ui->cmd_buf_pos] = 0;
    }
}

static void cmd_buf_reset(struct va_ui *ui) {
    memset(ui->cmd_buf, 0, CMD_LEN_MAX);
    ui->cmd_buf_pos = 0;
}

static void exec(struct va_ui *ui, struct command command) {
    if (command.type == CMD_QUIT) {
        ui->quit = true;
    }
}

static void draw_track_patterns(struct va_ui *ui) {
    char output[OUTPUT_BUF_LEN];

    int x, y;

    for (int i = 0; i < NUM_TRACKS; ++i) {
        struct va_track *track = &ui->seq->tracks[i];
        struct pattern_seq *pattern_seq = &ui->seq->vap.pattern_seq[i];
        uint8_t pattern_seq_idx = track->pattern_seq_idx;
        uint8_t pattern_idx = pattern_seq->pattern_ids[pattern_seq_idx];

        sprintf(output, "Track %d pattern: %u", i, pattern_idx);

        x_y_from_col_row(0, 3 + i, &x, &y);
        draw_text(ui, x, y, output);
    }
}

struct va_ui * va_ui_create(struct va_seq *seq, struct va_config *config) {

    struct va_ui *ui = malloc(sizeof(struct va_ui));

    ui->seq = seq;

    cmd_buf_reset(ui);

    uint32_t window_flags = 0;

    if (config->window_resizable) {
        window_flags = SDL_WINDOW_RESIZABLE;
    }

    if (SDL_CreateWindowAndRenderer(win_width, win_height, window_flags,
        &ui->window, &ui->renderer)) {

        log_error("error\n");
    };

    TTF_Init();

    TTF_Font *font = TTF_OpenFont("DejaVuSansMono.ttf", font_size);

    if (!font) {
        log_error("FAILED\n");
    }

    ui->font = font;
    ui->fg = (SDL_Color) {200, 200, 200};
    ui->bg = (SDL_Color) {0, 0, 50};

    ui->quit = false;

    return ui;
}

void va_ui_free(struct va_ui *ui) {
    TTF_CloseFont(ui->font);
    SDL_DestroyRenderer(ui->renderer);
    SDL_DestroyWindow(ui->window);

    free(ui);
    ui = NULL;

    TTF_Quit();
}

void va_ui_input(struct va_ui *ui) {
    log_debug("UI input.\n");

    SDL_Event keyboard_evt;
    while (SDL_PollEvent(&keyboard_evt)) {
        log_debug("Handle SDL event 0x%04x.\n", keyboard_evt.type);

        if (keyboard_evt.type == SDL_KEYDOWN) {
            SDL_Keycode keycode = keyboard_evt.key.keysym.sym;
            SDL_Keymod mod = SDL_GetModState();

            log_info("Key down mod %d keycode %d.\n", mod, keycode);

            if (keycode == SDLK_BACKSPACE) {
                cmd_buf_remove(ui);
            } else if (keycode == SDLK_RETURN || keycode == SDLK_RETURN2) {
                log_info("UI command %s\n", ui->cmd_buf);

                struct command command;
                result res = va_cmd_from_str(ui->cmd_buf, &command);

                if (res == OK) {
                    exec(ui, command);

                    va_seq_exec(ui->seq, command);
                }

                cmd_buf_reset(ui);
            } else if (mod & KMOD_SHIFT) {
                if (keycode == SDLK_SEMICOLON) {
                    keycode = SDLK_COLON;

                    cmd_buf_add(ui, keycode);
                }
            } else if (mod == KMOD_NONE) {
                cmd_buf_add(ui, keycode);
            }
        }
    }
}

void va_ui_show(struct va_ui *ui) {
    log_debug("UI show.\n");

    SDL_RenderClear(ui->renderer);

    int x, y;

    if (ui->cmd_buf[0] != 0) {
        x_y_from_col_row(0, 0, &x, &y);
        draw_text(ui, x, y, ui->cmd_buf);
    }

    char output[OUTPUT_BUF_LEN];

    sprintf(output, "Play position: %d", ui->seq->play_pos);

    x_y_from_col_row(0, 1, &x, &y);
    draw_text(ui, x, y, output);

    draw_track_patterns(ui);

    SDL_SetRenderDrawColor(ui->renderer, 0x0f, 0x00, 0x00, 0x00);
    SDL_RenderPresent(ui->renderer);
}
