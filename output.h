/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit resamples audio data and delivers it to the default audio device
/// at a rate of SAMPLES_PER_SEC KHz.
///
/// Create the output instance with the frequency of the incoming sample data
/// (e.g. 1 MHz). 

#ifndef OUTPUT_H
#define OUTPUT_H

#include <SDL2/SDL.h>

#define SAMPLES_PER_SEC 40000
#define SDL_BUFFER_LEN 2000

struct va_output {
    SDL_AudioSpec want, have;
    SDL_AudioDeviceID dev;

    /// Rate of incoming samples, in Hz.
    uint32_t clock_freq;
    uint64_t clock;
    uint8_t sdl_buffer[SDL_BUFFER_LEN];
    int bufpos;
};

/// Create a new output device. clock_freq is the rate of incoming samples,
/// in Hz.
struct va_output * va_output_create(uint32_t clock_freq);

/// Free the output device.
void va_output_free(struct va_output *output);

/// Start output device playback.
void va_output_play(struct va_output *output);

/// Send a single sample to the output device.
void va_output_sample(struct va_output *output, uint8_t sample);

#endif
