/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pattern.h"

#include <stddef.h>
#include <stdlib.h>

void va_pattern_init(struct pattern *pattern) {
    for (int i = 0; i < PATTERN_LEN; ++i) {
        pattern->notes[i] = NULL;
    }
}

void va_pattern_reset(struct pattern *pattern) {
    pattern->name[0] = 0;
    pattern->len = 0;

    for (int i = 0; i < PATTERN_LEN; ++i) {
        struct note *note = pattern->notes[i];

        free(note);
        pattern->notes[i] = NULL;
    }
}
