/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "vap.h"

#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "error.h"
#include "instrument.h"
#include "log.h"
#include "note.h"
#include "pattern.h"
#include "voice.h"

void va_vap_init(struct va_vap *vap) {
    for (int i = 0; i < NUM_PATTERNS; ++i) {
        va_pattern_init(&vap->patterns[i]);
    }
}

void va_vap_inst_reset(struct va_vap *vap) {
    for (int i = 0; i < NUM_INSTRUMENTS; ++i) {
        vap->instruments[i].waveform = PULSE;
        vap->instruments[i].duty_cycle = 0.5;
        vap->instruments[i].release = 0;
    }
}

void va_vap_patterns_reset(struct va_vap *vap) {
    for (int i = 0; i < NUM_PATTERNS; ++i) {
        va_pattern_reset(&vap->patterns[i]);
    }
}

void va_vap_pattern_seq_reset(struct va_vap *vap) {
    for (int t_idx = 0; t_idx < NUM_TRACKS; ++t_idx) {
        struct pattern_seq *pattern_seq = &vap->pattern_seq[t_idx];
        pattern_seq->len = 0;
    }
}

result va_vap_inst_create(struct va_vap *vap, uint8_t id,
    enum waveform waveform, double duty_cycle, uint8_t release) {

    result res = OK;

    if (id >= NUM_INSTRUMENTS) {
        log_error("Instrument idx %d exceeds limit.\n", id);
        res = ERROR_BOUNDS;
    }

    if (res == OK) {
        vap->instruments[id].waveform = waveform;
        vap->instruments[id].duty_cycle = duty_cycle;
        vap->instruments[id].release = release;
    }

    return res;
}

result va_vap_pattern_get(struct va_vap *vap, const char *name,
    uint8_t *pattern_id) {

    result res = ERROR_BOUNDS;

    for (int i = 0; i < NUM_PATTERNS; ++i) {
        struct pattern *pattern = &vap->patterns[i];

        if (strcasecmp(pattern->name, name) == 0) {
            if (pattern_id != NULL) {
                *pattern_id = i;
            }
            res = OK;
            break;
        }
    }

    return res;
}

result va_vap_pattern_add(struct va_vap *vap, const char *name,
    uint8_t *pattern_id) {

    result res = ERROR_BOUNDS;

    for (int i = 0; i < NUM_PATTERNS; ++i) {
        struct pattern *pattern = &vap->patterns[i];

        if (pattern->name[0] == 0) {
            strcpy(pattern->name, name);
            if (pattern_id != NULL) {
                *pattern_id = i;
            }
            res = OK;
            break;
        }
    }

    return res;
}

result va_vap_pattern_note_add(struct va_vap *vap, uint8_t pattern_id,
    uint8_t instrument_id, const char *pitch_name, enum effect effect,
    uint8_t effect_val) {

    result res = OK;

    struct pattern *pattern = &vap->patterns[pattern_id];

    if (pattern->len >= PATTERN_LEN) {
        log_error("Pattern len %d exceeds limit.\n", pattern->len);
        res = ERROR_BOUNDS;
    }
    if (pattern_id >= NUM_PATTERNS) {
        log_error("Pattern idx %d exceeds limit.\n", pattern_id);
        res = ERROR_BOUNDS;
    }
    if (instrument_id >= NUM_INSTRUMENTS) {
        log_error("Instrument idx %d exceeds limit.\n", instrument_id);
        res = ERROR_BOUNDS;
    }

    struct pitch pitch;

    if (res == OK) {
        res = pitch_find(&pitch, pitch_name);
    }

    if (res == OK) {
        struct note *note = malloc(sizeof(struct note));

        note->instrument_id = instrument_id;
        note->pitch = pitch;
        note->effect = effect;
        note->effect_val = effect_val;

        pattern->notes[pattern->len] = note;
        pattern->len += 1;
    }

    return res;
}

result va_vap_pattern_rest_add(struct va_vap *vap, uint8_t pattern_idx) {
    result res = OK;

    struct pattern *pattern = &vap->patterns[pattern_idx];

    if (pattern->len >= PATTERN_LEN) {
        log_error("Pattern len %d exceeds limit.\n", pattern->len);
        res = ERROR_BOUNDS;
    }

    if (res == OK) {
        pattern->len += 1;
    }

    return res;
}

result va_vap_pattern_seq_add(struct pattern_seq *pattern_seq,
    uint8_t pattern_idx) {

    result res = OK;

    if (pattern_seq->len >= PATTERN_SEQ_LEN) {
        log_error("Pattern seq len %d exceeds limit.\n", pattern_seq->len);
        res = ERROR_BOUNDS;
    }

    if (res == OK) {
        pattern_seq->pattern_ids[pattern_seq->len] = pattern_idx;
        pattern_seq->len += 1;
    }

    return res;
}
