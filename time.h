/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides facilities for dealing with time.

#ifndef TIME_H
#define TIME_H

#include <sys/time.h>

/// Subtract the struct timeval values X and Y, storing the result in RESULT.
/// Return 1 if the difference is negative, otherwise 0.
int timeval_subtract (struct timeval *result, struct timeval *x,
    struct timeval *y);

#endif
