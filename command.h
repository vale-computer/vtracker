/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit defines commands to control the sequencer. Commands may have zero
/// or more arguments.

#ifndef COMMAND_H
#define COMMAND_H

#include <stdint.h>

#include "error.h"

#define NUM_CMD_ARGS 4

enum command_type {
    CMD_LOAD,
    CMD_RUN,
    CMD_STOP,
    CMD_SEEK,
    CMD_LOOP0,
    CMD_LOOP1,
    CMD_QUIT,
};

struct command {
    /// Command type.
    enum command_type type;
    /// Command arguments.
    uint32_t arguments[NUM_CMD_ARGS];
    /// Number of command arguments.
    uint8_t args_len;
};

/// Convert cmd_str to the associated command. Return it in command.

/// cmd_str is a white-space delimited string, where the first token is the
/// type of the command and the rest of the tokens are the command's arguments.
result va_cmd_from_str(char *cmd_str, struct command *command);

#endif
