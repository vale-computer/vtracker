CFLAGS ?= -g -std=c99 -pedantic -Wall

sources = clock.c command.c config.c output.c pattern.c pitch.c sdl.c \
	sequencer.c time.c ui.c vap.c vap_parser.c vau.c voice.c vtracker.c

objects = $(sources:.c=.o)
deps = $(sources:.c=.d)

.PHONY: all
all: vtracker

.PHONY: clean
clean:
	$(RM) $(objects) $(deps)

vtracker: $(objects)
	$(CC) $(CFLAGS) -lm -lSDL2 -lSDL2_ttf $^ -o $@

%.d: %.c
	@set -e; $(RM) $@; \
	    $(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	    sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	    $(RM) $@.$$$$

include $(deps)
