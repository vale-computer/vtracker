/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "config.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "error.h"
#include "log.h"

#define CFG_LINE_LEN_MAX 64

const char *config_default_text[] = {
    "; This is the number of steps to execute for each run of the audio system.\n",
    "; A smaller value makes the application more responsive (good).\n",
    "; Too small causes choppy audio (bad).\n",
    "sample_ahead 75000\n",
    "; This is the calendar duration of audio time left in the output buffer at\n",
    "; which the application triggers another run of the audio system.\n",
    "; A smaller value keeps the UI better synchronized with the audio (good).\n",
    "; Too small causes choppy audio (bad).\n",
    "sample_at 100000\n",
    "; If true, application windows are resizable.\n",
    "window_resizable true\n",
};

#define config_default_text_len() sizeof(config_default_text) / \
    sizeof(const char *)

static result bool_parse(const char *input, bool *output) {
    result res = OK;

    if (strcasecmp(input, "true") == 0) {
        *output = true;
    } else if (strcasecmp(input, "false") == 0) {
        *output = false;
    } else {
        res = ERROR_BOUNDS;
    }

    return res;
}

static result int_parse(const char *input, long int *output) {
    result res = OK;

    char *tail;

    *output = strtol(input, &tail, 0);

    if (tail[0] != 0) {
        res = ERROR_BOUNDS;
    }

    return res;
}

result config_load(struct va_config *config, const char *filename) {
    log_info("Load configuration from %s.\n", filename);

    result res = OK;

    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        log_error("Failed to open %s.\n", filename);
        res = ERROR_IO;
    }

    char line[CFG_LINE_LEN_MAX];

    if (res == OK) {
        while (fgets(line, CFG_LINE_LEN_MAX, file)) {
            int len = strlen(line);
            line[len - 1] = 0;

            log_debug("Parse line %s\n", line);

            if (strlen(line) == 0 || line[0] == ';') {
                log_debug("Skip empty or comment line.\n");
                continue;
            }

            char *tok = strtok(line, " ");

            if (strcasecmp(tok, "sample_ahead") == 0) {
                if ((tok = strtok(NULL, " ")) == NULL) {
                    log_error("Missing sample_ahead value.\n");
                    res = ERROR_BOUNDS;
                    break;
                }

                if ((res = int_parse(tok, (long int*) &config->sample_ahead))
                        != OK) {
                    log_error("Failed to parse sample_ahead value %s.\n", tok);
                    break;
                }
            } else if (strcasecmp(tok, "sample_at") == 0) {
                if ((tok = strtok(NULL, " ")) == NULL) {
                    log_error("Missing sample_at value.\n");
                    res = ERROR_BOUNDS;
                    break;
                }

                if ((res = int_parse(tok, (long int*) &config->sample_at))
                        != OK) {
                    log_error("Failed to parse sample_ahead value %s.\n", tok);
                    break;
                }
            } else if (strcasecmp(tok, "window_resizable") == 0) {
                if ((tok = strtok(NULL, " ")) == NULL) {
                    log_error("Missing window_resizable value.\n");
                    res = ERROR_BOUNDS;
                    break;
                }

                if ((res = bool_parse(tok, &config->window_resizable))
                        != OK) {
                    log_error("Failed to parse window_resizable value %s.\n", tok);
                    break;
                }
            }

        }

        if (res == OK) {
            log_info("Sample ahead: %d.\n", config->sample_ahead);
            log_info("Sample at: %li.\n", config->sample_at);
            log_info("Window resizable: %d.\n", config->window_resizable);
        }
    }

    if (file != NULL) {
        if (fclose(file) != 0) {
            log_error("Failed to close %s.\n", filename);
            res = ERROR_IO;
        }
    }

    if (res != OK) {
        log_error("Failed to load configuration from %s.\n", filename);
    }

    return res;
}

result config_default(const char *filename) {
    log_info("Create default configuration %s.\n", filename);

    result res = OK;

    FILE *file = fopen(filename, "w");

    if (file == NULL) {
        log_error("Failed to open %s.\n", filename);
        res = ERROR_IO;
    }

    for (int i = 0; i < config_default_text_len(); ++i) {
        fprintf(file, config_default_text[i]);
    }

    if (file != NULL) {
        if (fclose(file) != 0) {
            log_error("Failed to close %s.\n", filename);
            res = ERROR_IO;
        }
    }

    if (res != OK) {
        log_error("Failed to create configuration file %s.\n", filename);
    }

    return res;
}
