/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit coordinates the playback of an audio project.

#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <stdbool.h>
#include <sys/time.h>

#include "command.h"
#include "error.h"
#include "note.h"
#include "pattern.h"
#include "vap.h"
#include "voice.h"

// Total number of available start commands.
#define NUM_COMMANDS 64

/// Current sequencer status.
enum va_seq_status {
    /// Running.
    SEQ_RUN,
    /// Stopped.
    SEQ_STOP,
};

/// A track represents a currently playing pattern in a pattern sequence.
struct va_track {
    bool enabled;

    /// Level (volume) of sound for this track.
    uint8_t level;

    /// Index of current pattern in the pattern sequence.
    uint8_t pattern_seq_idx;

    /// Index of currently playing pattern.
    uint8_t pattern_id;
    /// Step of currently playing pattern.
    uint8_t pattern_step;

    /// Currently applied effect.
    enum effect effect;
    /// Value of currently applied effect.
    uint8_t effect_val;
};


/// A sequencer contains instruments and patterns, which are independent of any
/// track. It also contains a track for each voice of the VAU.
struct va_seq {
    /// Current VAP project.
    struct va_vap vap;

    /// Tracks.
    struct va_track tracks[NUM_TRACKS];

    enum va_seq_status status;
    uint32_t clock_freq;
    uint64_t clock;
    struct vau *vau;

    /// Loop begin position.
    uint32_t loop_0;
    /// Loop end begin.
    uint32_t loop_1;

    /// Position of the playhead in the project. The maximum position of
    /// the playhead is the length of the longest of the tracks.
    uint32_t play_pos;

    /// Filename of the current project.
    const char *filename;
};

/// Create a new sequencer.
struct va_seq * va_seq_create(struct vau *vau, uint32_t clock_freq);

/// Free the sequencer.
void va_seq_free(struct va_seq *seq);

/// Load a project from a file.
result va_seq_load(struct va_seq *seq, const char *filename);

/// Run the sequencer.
void va_seq_run(struct va_seq *seq);

/// Stop the sequencer.
void va_seq_stop(struct va_seq *seq);

/// Set the sequencer playhead to a position.
void va_seq_seek(struct va_seq *seq, uint32_t play_pos);

/// Update the state of the sequencer.
void va_seq_step(struct va_seq *seq);

/// Get the sample, mixed from all the sequencer tracks.
uint8_t va_seq_sample(struct va_seq *seq);

/// Execute a command.
void va_seq_exec(struct va_seq *seq, struct command command);

#endif
