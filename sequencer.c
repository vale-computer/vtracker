/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sequencer.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "command.h"
#include "error.h"
#include "log.h"
#include "pattern.h"
#include "pitch.h"
#include "vap.h"
#include "vap_parser.h"
#include "vau.h"

#define TRACK_LEVEL_MAX 0xf

static void tracks_reset(struct va_seq *seq) {
    for (int t_idx = 0; t_idx < NUM_TRACKS; ++t_idx) {
        struct va_track *track = &seq->tracks[t_idx];

        track->enabled = false;
        track->level = TRACK_LEVEL_MAX;
        track->pattern_seq_idx = 0;
        track->pattern_id = 0;
        track->pattern_step = 0;
        track->effect = NONE;
        track->effect_val = 0;
    }
}

struct va_seq * va_seq_create(struct vau *vau, uint32_t clock_freq) {
    struct va_seq *seq = malloc(sizeof(struct va_seq));

    seq->status = SEQ_STOP;
    seq->clock = 0;
    seq->clock_freq = clock_freq;
    seq->vap.beats_per_min = 60;
    seq->loop_0 = 0;
    seq->loop_1 = 0;
    seq->play_pos = 0;

    va_vap_init(&seq->vap);

    va_vap_inst_reset(&seq->vap);
    va_vap_patterns_reset(&seq->vap);
    va_vap_pattern_seq_reset(&seq->vap);
    tracks_reset(seq);

    seq->vau = vau;

    return seq;
}

void va_seq_free(struct va_seq *seq) {
    va_vap_patterns_reset(&seq->vap);

    free(seq);
    seq = NULL;
}

result va_seq_load(struct va_seq *seq, const char *filename) {
    result res = OK;

    if (filename != NULL) {
        seq->filename = filename;
    }

    va_vap_inst_reset(&seq->vap);
    va_vap_patterns_reset(&seq->vap);
    va_vap_pattern_seq_reset(&seq->vap);
    tracks_reset(seq);

    res = va_vap_seq_load(seq, seq->filename);

    if (res == OK) {
        res = va_vap_track_load(seq, seq->filename);
    }

    return res;
}

void va_seq_run(struct va_seq *seq) {
    seq->status = SEQ_RUN;
}

void va_seq_stop(struct va_seq *seq) {
    seq->status = SEQ_STOP;
}

void va_seq_seek(struct va_seq *seq, uint32_t play_pos) {
    log_info("Seek position %u.\n", play_pos);

    for (int ch_idx = 0; ch_idx < NUM_TRACKS; ++ch_idx) {
        int pattern_end_global_pos = 0;

        struct va_track *track = &seq->tracks[ch_idx];
        struct pattern_seq *pattern_seq = &seq->vap.pattern_seq[ch_idx];

        for (int pat_id_idx = 0; pat_id_idx < pattern_seq->len; ++pat_id_idx) {
            int pattern_id = pattern_seq->pattern_ids[pat_id_idx];

            struct pattern *pattern = &seq->vap.patterns[pattern_id];
            pattern_end_global_pos += pattern->len;

            log_debug("Try track %d pattern idx %d pattern end pos %d.\n",
                ch_idx, pat_id_idx, pattern_end_global_pos);

            if (play_pos < pattern_end_global_pos) {
                // This is the right pattern sequence.
                int step = play_pos - (pattern_end_global_pos - pattern->len);

                log_debug("Go to pattern idx %d pattern %u step %u.\n",
                    pat_id_idx, pattern_id, step);

                track->pattern_id = pattern_id;
                track->pattern_step = step;
                track->pattern_seq_idx = pat_id_idx;

                break;
            }
        }
    }

    seq->play_pos = play_pos;
}

// Find the global end position of the song.
uint32_t end_pos(struct va_seq *seq) {
    int channel_end_pos[NUM_TRACKS];

    // Find global end position of each track's pattern sequence.

    for (int chan_idx = 0; chan_idx < NUM_TRACKS; ++chan_idx) {
        struct va_track *track = &seq->tracks[chan_idx];
        struct pattern_seq *pattern_seq = &seq->vap.pattern_seq[chan_idx];

        int end_pos = 0;

        for (int pattern_seq_idx = 0; pattern_seq_idx < pattern_seq->len;
            ++pattern_seq_idx) {

            int pattern_idx = pattern_seq->pattern_ids[pattern_seq_idx];
            struct pattern *pattern = &seq->vap.patterns[pattern_idx];

            end_pos += pattern->len;
        }
        channel_end_pos[chan_idx] = end_pos;
        
    }

    // Sort least to greatest.

    bool sort;

    do {
        sort = false;

        for (int i = 0; i < NUM_TRACKS - 1; ++i) {
            if (channel_end_pos[i + 1] < channel_end_pos[i]) {
                int temp = channel_end_pos[i];
                channel_end_pos[i] = channel_end_pos[i + 1];
                channel_end_pos[i + 1] = temp;

                sort = true;
            }
        }
    } while (sort);

    log_info("End pos %d %d %d %d.\n", channel_end_pos[0], channel_end_pos[1],
        channel_end_pos[2], channel_end_pos[3]);

    return channel_end_pos[3];
}

static void tracks_update(struct va_seq *seq) {
    // Process each track in turn.

    if (seq->loop_1 != 0 && seq->play_pos >= seq->loop_1 - 1) {
        log_info("Loop from %u to %u.\n", seq->loop_1, seq->loop_0);
        va_seq_seek(seq, seq->loop_0);
    } else {
        for (int i = 0; i < NUM_TRACKS; ++i) {
            log_info("Track update %d.\n", i);

            struct va_track *track = &seq->tracks[i];

            if (!track->enabled) {
                log_info("Skip disabled track.\n");
                continue;
            }

            // Get the currently playing pattern for the track.

            uint8_t pattern_id = track->pattern_id;

            log_info("Track %u pattern_id %u pattern_step %u.\n", i,
                pattern_id, track->pattern_step);

            // Advance the pattern step.

            track->pattern_step += 1;

            if (track->pattern_step >= seq->vap.patterns[pattern_id].len) {
                // The new pattern step is past the length of the current pattern.
                // Change the pattern to the next pattern in the track's pattern
                // sequence.

                track->pattern_seq_idx += 1;
                struct pattern_seq *pattern_seq = &seq->vap.pattern_seq[i];

                log_debug("End pattern. New pattern seq idx %d.\n",
                    track->pattern_seq_idx);

                track->pattern_step = 0;

                if (track->pattern_seq_idx >= pattern_seq->len) {
                    // Loop around to the beginning of the track's pattern
                    // sequence.

                    log_info("Track pattern loop\n.");
                    track->pattern_seq_idx = 0;
                    track->pattern_id = pattern_seq->pattern_ids[
                        track->pattern_seq_idx];
                    log_info("End of pattern sequence; disable.\n");
                    track->enabled = false;
                } else {
                    // Advance the track pattern sequence, reset the pattern step
                    // and set the track's currently playing pattern to the next
                    // one in the sequence.
                    log_info("Track pattern next seq %u.\n",
                        track->pattern_seq_idx);
                }

                track->pattern_id = pattern_seq->pattern_ids[
                    track->pattern_seq_idx];
            }
        }

        seq->play_pos += 1;
    }
}

static void va_vau_update(struct va_seq *seq) {
    // Process each track in turn.

    for (int i = 0; i < NUM_TRACKS; ++i) {
        log_debug("VA update track %d.\n", i);

        struct va_track *track = &seq->tracks[i];

        if (!track->enabled) {
            log_info("Skip disabled track.\n");
            continue;
        }

        // Get the next note of the currently playing pattern for this track.

        uint8_t pattern_id = track->pattern_id;

        log_debug("Pattern id %d.\n", pattern_id);

        struct pattern *pattern = &seq->vap.patterns[pattern_id];
        struct note *note = pattern->notes[track->pattern_step];

        if (note != NULL) {
            // Set the track effect.

            track->effect = note->effect;
            track->effect_val = note->effect_val;

            // Get the new VA voice settings from the note and corresponding
            // instrument.

            log_info("Note instrument %u pitch %s.\n", note->instrument_id,
                note->pitch.pitch_name);

            enum waveform waveform = seq->vap.instruments[note->instrument_id].
                waveform;
            double duty_cycle = seq->vap.instruments[note->instrument_id].
                duty_cycle;
            double frequency = note->pitch.frequency;
            uint8_t release = seq->vap.instruments[note->instrument_id].release;

            log_debug("VA set track %d waveform %d duty cycle %f frequency %f release %u.\n",
                i, waveform, duty_cycle, frequency, release);

            // Set the new VA voice settings.

            // Reinitialize the voice. The voices become unsynchronized over
            // time, which causes samples to cancel when they're mixed. TODO:
            // fix the problem that causes voices to become unsynchronized.

            vau_voice_init(seq->vau, i);
            vau_wave(seq->vau, i, waveform, duty_cycle);
            vau_freq(seq->vau, i, frequency);
            vau_adsr(seq->vau, i, 0, 0, 0, release);
        } else {
            log_debug("VA no change.\n");
        }
    }
}

static void notes_update(struct va_seq *seq) {
    log_info("Notes update.\n");

    for (int i = 0; i < NUM_TRACKS; ++i) {
        struct va_track *track = &seq->tracks[i];

        if (track->effect == PITCH_SLIDE) {
            double freq = vau_freq_get(seq->vau, i);
            freq -= track->effect_val;
            vau_freq(seq->vau, i, freq);
        }
    }
}

void va_seq_step(struct va_seq *seq) {
    // A pattern is up to 64 divisions.
    // Each quarter note is a beat. Every 4 divisions is a quarter note.
    // Every division is a 16th note.
    //
    // At 60 beats per minute, there are 240 divisions per minute and 4
    // divisions per second.

    double div_per_min = seq->vap.beats_per_min * 4.0;
    double div_per_min2 = seq->vap.beats_per_min * 16.0;
    double div_per_sec = div_per_min / 60.0;
    double div_per_sec2 = div_per_min2 / 60.0;
    uint32_t clocks_per_div = seq->clock_freq / div_per_sec;
    uint32_t clocks_per_div2 = seq->clock_freq / div_per_sec2;

    if (seq->status == SEQ_RUN) {
        if (seq->clock % clocks_per_div == 0) {
            log_info("Play position %u.\n", seq->play_pos);
            log_info("Clock %lu clocks/div %u.\n", seq->clock, clocks_per_div);

            // Update the VA device using the current state of each track's
            // pattern.

            va_vau_update(seq);
            tracks_update(seq);
        }

        if (seq->clock % clocks_per_div2 == 0) {
            notes_update(seq);
        }

        seq->clock += 1;

        vau_step(seq->vau);
    }
}

uint8_t va_seq_sample(struct va_seq *seq) {
    uint8_t samples[4] = {0, 0, 0, 0};

    for (int i = 0; i < NUM_TRACKS; ++i) {
        struct va_track *track = &seq->tracks[i];

        if (track->enabled) {
            samples[i] = vau_ampl(seq->vau, i);
        }
    }

    uint8_t mixed = (0.25 * (samples[0] * (&seq->tracks[0])->level / TRACK_LEVEL_MAX) +
        0.25 * (samples[1] * (&seq->tracks[1])->level / TRACK_LEVEL_MAX) +
        0.25 * (samples[2] * (&seq->tracks[2])->level / TRACK_LEVEL_MAX) +
        0.25 * (samples[3] * (&seq->tracks[3])->level / TRACK_LEVEL_MAX));

    log_trace("Samples ch0 %u ch1 %u ch2 %u ch3 %u mixed %u.\n", samples[0],
        samples[1], samples[2], samples[3], mixed);

    return mixed;
}

void va_seq_exec(struct va_seq *seq, struct command command) {
    log_info("Execute command.\n");

    if (command.type == CMD_LOAD) {
        log_info("Load.\n");

        va_seq_stop(seq);
        va_seq_load(seq, NULL);
    } else if (command.type == CMD_RUN) {
        log_info("Run.\n");

        va_seq_run(seq);
    } else if (command.type == CMD_STOP) {
        log_info("Stop.\n");

        va_seq_stop(seq);
    } else if (command.type == CMD_SEEK) {
        int pos = command.arguments[0];

        log_info("Seek %d.\n", pos);

        va_seq_seek(seq, pos);
    } else if (command.type == CMD_LOOP0) {
        int val = command.arguments[0];

        log_info("Loop 0 %d.\n", val);

        seq->loop_0 = val;
    } else if (command.type == CMD_LOOP1) {
        int val = command.arguments[0];

        log_info("Loop 1 %d.\n", val);

        seq->loop_1 = val;
    }
}
