/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides the parser for the VALE Audio Project (VAP) file format.
///
/// The parser assumes that VAP files may be modified by hand, and attempts to
/// handle as many common error cases as possible.

#ifndef VAP_PARSER
#define VAP_PARSER

#include "error.h"
#include "sequencer.h"

// Load sequencer state into the sequencer from a file.
result va_vap_seq_load(struct va_seq *seq, const char *filename);

// Load a track into the sequencer from a file.
result va_vap_track_load(struct va_seq *seq, const char *filename);

#endif
