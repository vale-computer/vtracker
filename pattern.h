/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit provides musical note patterns.

#ifndef PATTERN_H
#define PATTERN_H

#include <stdint.h>

// Maximum length of a pattern name.
#define PATTERN_NAME_LEN 32
// Maximum length of a pattern.
#define PATTERN_LEN 64

/// A pattern is an ordered sequence of notes which can be up to PATTERN_LEN
/// in length.
struct pattern {
    /// Name.
    char name[PATTERN_NAME_LEN];
    /// Sequence of notes.
    struct note *notes[PATTERN_LEN];
    /// Number of steps (length of pattern sequence).
    uint8_t len;
};

/// Initialize it. Call this before doing anything else.
void va_pattern_init(struct pattern *pattern);

/// Reset it to default state.
void va_pattern_reset(struct pattern *pattern);

#endif
