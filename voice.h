/*
Copyright (C) 2018-2019 Francis (Fritz) Mahnke.

This file is part of VALE8x64.

VALE8x64 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VALE8x64 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VALE8x64.  If not, see <https://www.gnu.org/licenses/>.
*/

/// This unit emulates an oscillator (voice) of the VALE Audio Unit (VAU).

#ifndef VOICE_H
#define VOICE_H

#include <stdint.h>

enum waveform {
    PULSE,
    SAWTOOTH,
    TRIANGLE,
    NOISE,
};

struct va_voice {
    // Type of waveform.
    enum waveform wave;
    // Instantaneous amplitude.
    uint8_t amplitude;
    // Frequency, in Hz.
    double frequency;
    // Duty cycle.
    double duty_cycle;

    double level;

    uint8_t release;
    uint64_t release_step;

    uint64_t step;

    // Clock frequency, in Hz.
    uint32_t clock_freq;
};

/// Create a new voice.
struct va_voice * va_voice_create(uint32_t clock_freq);

/// Free the voice.
void va_voice_free(struct va_voice *voice);

/// Update the state of the voice.
void va_voice_step(struct va_voice *voice);

/// Set the waveform of a voice.
void va_voice_wave(struct va_voice *voice, enum waveform waveform,
    double duty_cycle);

/// Set the frequency of a voice.
void va_voice_freq(struct va_voice *voice, double frequency);

/// Initialize a voice.
void va_voice_init(struct va_voice *voice);

/// Get a sample of audio from a voice.
uint8_t va_voice_ampl(struct va_voice *voice);

/// Set the attack/decay/sustain/release (ADSR) envelope of a voice.
void va_voice_adsr(struct va_voice *voice, uint8_t attack, uint8_t decay,
    uint8_t sustain, uint8_t release);

#endif
